
# Dinoco's Veloren Voxel Design Contribution GitLab Depository

## Description
This Project is meant to provide some contribution for a game already in development "Veloren" (Project ID: 10174980).
The Models are made in MagicaVoxel and are also shared in the Veloren Official Discord Server (in #veloren-art) and submmited in the Game Concepts GoogleDocs.

## Roadmap
The current branches in development are:
### General Assets
	# Dwarven Minecart: WIP
		# Variations: -
	# Minecart Rails: -
	# Blacksmith Tools: -
		# Types: -
### Neutral Mobs
	# Red Panda: Complete (2a25fb99)
	# Lemur: WIP
### Hostile Mobs
	# Gigantopithicus: -
### Boss Mobs
### NPC's
### Equipment

## Contributing
I accept requests and ideas from any source. Credit is given where it's due.

## Author
Dialex "Dinoco" Neves

## License
GNU General Public License v3.0

## Project status
Always active as long as Veloren accepts contributions.
Additions to the project may not me regular, as some require more time and work put into.
